#!/bin/bash
clear

cd /opt/whatsapp/

echo "Gateway Whatsapp v0.3"
echo "----------------------"
echo "REINICIAR GATEWAY"
echo "----------------------"
pm2 stop all
rm sessions/*
pm2 start all
clear

pm2 status
