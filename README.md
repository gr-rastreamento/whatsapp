# Gateway Whatsapp Traccar API

Gateway Whatsapp integrado com a plataforma de rastreamento Traccar.

## Instalação
Instale o pacote de desenvolvimento NODEJS
Ubuntu
```bash
sudo apt-get update
sudo apt-get install build-essential libssl-dev
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
```

Copie e cole o texto abaixo no console do linux para registro do nvm no sistema
```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

E finalmente execute a instalação do NodeJS
```bash
nvm install node
```

CentOS
```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
```

Copie e cole o texto abaixo no console do linux para registro do nvm no sistema
```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

E finalmente execute a instalação do NodeJS
```bash
nvm install node
```

Realize download do repositório da API para instalação
```bash
cd /opt
git clone https://github.com/michaelloliveira/whatsapp-gateway-single.git
mv whatsapp-gateway-single whatsapp
cd whatsapp
npm i
```

## Configuração
Edite o arquivo de configurações
```bash
nano /opt/whatsapp/config/enviroment.js
```

# SSL
Para uso com SSL realize as tarefas nos links abaixo de acordo com o seu sistema operacional

Ubuntu
```bash
https://certbot.eff.org/lets-encrypt/ubuntufocal-other
```

CentOS
```bash
https://certbot.eff.org/lets-encrypt/centos6-other
```

# Configurar Traccar
Inclua o código abaixo no arquivo /opt/traccar/conf/traccar.xml
Nota: substitua os valores das chaves com os valores referentes ao seu servidor.

```bash
<!-- Notificacoes por whatsapp -->
<entry key='notificator.types'>whatsapp</entry>
<entry key='notificator.whatsapp.url'>http://seudns_ou_ip</entry>
<entry key='notificator.whatsapp.port'>3031</entry>
<entry key='notificator.whatsapp.token'>9943b52063cd54097a65d1633f5c74d3</entry>
```
Acesse a url combinada com a porta para vincular o QR Code

```bash
Ex: http://seudns_ou_ip:3031/qr/seutoken
```


## Executar
Em segundo Plano
```bash
screen
cd /opt/whatsapp
node app.js
CTRL + A + D
```

Com gerenciador de processos Node
```bash
npm i pm2 -g
bash /opt/whatsapp/reset_gateway.sh
```

## Reiniciar QR Code
É possível que o serviço whatsapp seja interrompido, o sistema pode ser reiniciado com o comando abaixo.
```bash
bash /opt/whatsapp/restart_gateway.sh
```

Em alguns casos será necessário gerar um novo QR. Realize a operação abaixo para gerar uma nova integração.
```bash
bash /opt/whatsapp/restart_qr.sh
```


## Desenvolvedor
Michaell Oliveira
+55 82 99987-7780
[Website](https://www.solutions2u.com.br/)

[Email](mailto://michaelloliveira@gmail.com)

[Linked](https://www.linkedin.com/in/michaell-oliveira-428a1792/)

[Instagram](https://www.instagram.com/michaell_oliveira_/?hl=pt-br)


## Licença
API Gateway não oficial Whatsapp
