
exports.enviroment = {
    name: 'Minha Empresa', //nome exibido no sistema
    port: 3001, //porta de acesso
    token: '9943b52063cd54097a65d1633f5c74d3', //token para permitir envio das mensagens
    ip: null, //caso queria limitar o envio por IP, inclua aqui o IP do seu servidor ex: '127.0.0.1'
    useSSL: false, // mude para true se deseja ativar o servidor web ssl (é necessário configurar o letscrypt)
    letscrypt: 'seudominio.noletscrypt.com' //dominio registrado ssl no seu servidor
}

