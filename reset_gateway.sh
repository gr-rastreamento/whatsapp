#!/bin/bash
clear

cd /opt/whatsapp/

echo "Gateway Whatsapp v0.3"
echo "----------------------"
echo "INICIAR GATEWAY"
echo "----------------------"
echo "Parando gateway..."
pm2 stop all
echo "Apagando instancia..."
pm2 del all
echo "Removendo sessao..."
rm sessions/*
echo "Criando servico..."
pm2 --name whatsapp --restart-delay 30000 start app.js
clear
echo "Registrando servico..."
pm2 save
pm2 status
